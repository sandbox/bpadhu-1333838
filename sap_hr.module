<?php

/**
 * @file
 * SAP HR Employee Information
 */

function sap_hr_perm() {
  return array(
    'access SAP',
    'configure SAP HR',
  );
}

/**
 * Implementation of hook_menu().
 */
function sap_hr_menu() {
  $items = array();

  $items['admin/settings/sap_hr_config'] = array(
    'title'            => 'Configure SAP HR Web Service',
    'description'      => 'Configure SAP HR Web Service Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_basic_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
  );

  $items['admin/settings/sap_hr_config/sap_hr_basic_employee'] = array(
    'title'            => 'Configure SAP HR Basic Employee',
    'description'      => 'Configure SAP HR Basic Employee Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_basic_employee_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 10,
  );

  $items['admin/settings/sap_hr_config/sap_hr_vacation_balance'] = array(
    'title'            => 'Configure SAP HR Employee Vacation Balance',
    'description'      => 'Configure SAP HR Employee Vacation Balance Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_vacation_balance_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 11,
  );

  $items['admin/settings/sap_hr_config/sap_hr_create_leave_request'] = array(
    'title'            => 'Configure SAP HR Employee Create Leave Request',
    'description'      => 'Configure SAP HR Employee Create Leave Request Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_create_leave_request_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 12,
  );

  $items['admin/settings/sap_hr_config/sap_hr_manager'] = array(
    'title'            => 'Configure SAP HR Employee Manager',
    'description'      => 'Configure SAP HR Employee Manager Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_manager_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 13,
  );

  $items['admin/settings/sap_hr_config/sap_hr_direct_reports'] = array(
    'title'            => 'Configure SAP HR Employee Direct Reports',
    'description'      => 'Configure SAP HR Employee Direct Reports Information',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_direct_reports_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 14,
  );

  $items['admin/settings/sap_hr_config/sap_hr_get_leave_request'] = array(
    'title'            => 'Configure SAP HR Employee Get Leave Requests',
    'description'      => 'Configure SAP HR Employee Get Leave Requests',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_get_leave_request_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 15,
  );

  $items['admin/settings/sap_hr_config/sap_hr_approve_leave_request'] = array(
    'title'            => 'Configure SAP HR Employee Approve Leave Request',
    'description'      => 'Configure SAP HR Employee Approve Leave Request',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_approve_leave_request_admin_settings'),
    'access callback'  => 'user_access',
    'access arguments' => array('configure SAP HR'),
    'file'             => 'sap_hr.admin.inc',
    'weight'           => 16,
  );

  $items['sapuser'] = array(
    'title' => 'SAP User Information',
    'page callback' => 'sapuser_view',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('access SAP'),
    'parent' => '',
    'type' => MENU_CALLBACK,
  );

  $items['sapuser/reports'] = array(
    'title' => 'SAP Direct Reports Information',
    'page callback' => 'sapdirect_report_view',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'access arguments' => array('access SAP'),
    'type' => MENU_CALLBACK,
    'parent' => '',
  );

  $items['sapuser/submitlr'] = array(
    'title' => 'SAP Submit Leave Request',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sap_hr_create_leave_request_user', array(2)),
    'access callback' => 'user_access',
    'access arguments' => array('access SAP'),
    'parent' => '',
    'type' => MENU_CALLBACK,
    'file' => 'sap_hr.user.inc',
  );

  $items['sapmanager'] = array(
    'title' => 'SAP Manager Information',
    'page callback' => 'sapmanager_view',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('access SAP'),
    'type' => MENU_CALLBACK,
    'parent' => '',
  );

  $items['sapmanager/checklr'] = array(
    'title' => 'SAP User Information',
    'page callback' => 'sapmanager_view_leaverequests',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access SAP'),
    'parent' => '',
  );

  $items['sapmanager/approve'] = array(
    'title' => 'SAP Approve Leave Request',
    'page callback' => 'sapmanager_approve_request',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access SAP'),
    'parent' => '',
  );

  return $items;
}

/**
 * Menu callback; Displays the sap user information.
 * Calls 3 webservices: Employee Information, ManageInformation and 
 * Vacation Balance Information.
 */
function sapuser_view($sap_employee_id = '') {
  if ($sap_employee_id == '') $sap_employee_id = '1000';
  $emp_info = getEmployeeInformation($sap_employee_id) . '<br>' . 
              getManagerInformation($sap_employee_id) . '<br>' .
              getVacationBalanceInformation($sap_employee_id);
  return $emp_info;
}

/**
 * Menu callback; Displays the sap user information.
 * Calls 3 webservices: Employee Information, ManageInformation and 
 * Vacation Balance Information.
 */
function sapmanager_view($sap_employee_id) {
  if ($sap_employee_id == '') $sap_employee_id = '1009';
  $emp_info = getEmployeeInformation($sap_employee_id) . '<br>' . getDirectReports($sap_employee_id);//.'<br>'.getManagerInformation($sap_employee_id);
  //$emp_info = $emp_info.'<br>'.getLeaveRequests($sap_employee_id);
  return $emp_info;
}

function sapmanager_view_leaverequests($sap_employee_id) {
  if ($sap_employee_id == '') $sap_employee_id = '1009';
  $emp_info = getLeaveRequests($sap_employee_id);
  return $emp_info;
}

function sapdirect_report_view($sap_employee_id) {
  return getDirectReports($sap_employee_id);
}

function sapmanager_approve_request($leave_request_id) {
  return approveLeaveRequest($leave_request_id);
}

function approveLeaveRequest($leave_request_id) {
  $wsdl_protocol  = variable_get('sap_hr_approve_leave_request_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_approve_leave_request_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_approve_leave_request_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_approve_leave_request_sap_request_xml', '');
  $response_arr = soapclient_invoke_approve_leave_request($leave_request_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  if ($response_arr['soap-env:body']['n0:employeebasicdatabyemployeeresponse']['employeeleaverequest']['lifecyclestatusname']['data'] == 'Approved')
    return t('Leave request Approved');
  else
    return t('Leave request could not be Approved');
}

function getEmployeeInformation($sap_employee_id) {
  $wsdl_protocol  = variable_get('sap_hr_employee_information_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_employee_information_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_employee_information_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_employee_information_sap_request_xml', '');

  $response_arr = soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  $employee_org_arr = $response_arr['soap-env:body']['n0:employeebasicdatabyemployeeresponse']['employee'];
  if ($employee_org_arr['id'] != '') {
    $employee[] = array('ID', $employee_org_arr['id']);
    $famname_str = $employee_org_arr['common']['name']['familyname'];
    $employee[] = array('Name', $employee_org_arr['common']['name']['formofaddressname'] . " " . 
                                $employee_org_arr['common']['name']['givenname'] . " ". $famname_str);
    $employee[] = array('Birth Place', $employee_org_arr['common']['birthplacename']);
    $employee[] = array('Birth Date', $employee_org_arr['common']['birthdate']);
    $nationality_code = $employee_org_arr['nationality']['nationalitycountrycode'];
    $employee[] = array('Nationality', $nationality_code == 'US' ? 'USA' : $nationality_code);
    $html_table = theme_table(array('ID', 'Value'), $employee, "");
    return $html_table;
  }
  return t('<p>No User Information</p>');
}

function getDirectReports($sap_employee_id) {
  $wsdl_protocol  = variable_get('sap_hr_direct_report_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_direct_report_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_direct_report_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_direct_report_sap_request_xml', '');

  $response_arr = soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  $reports_org_arr = $response_arr['soap-env:body']['n0:reportingemployeebyemployeeresponse'];

  if (sizeof($reports_org_arr) > 3) {
    $header = array('Employee ID', 'Employee Name', 'Position', 'Department');
    $employee = array();
    for ($i = 0; $i < sizeof($reports_org_arr) - 3; $i++) {
      $directreport = $reports_org_arr[strval($i)];
      $employee[] = array($directreport['id'], $directreport['personformattedname'], $directreport['employeeassignment']['position']['description'], $directreport['employeeassignment']['position']['organisationalcentreassigmnent']['organisationalcentrename']);
    }
    $html_table = theme_table($header, $employee, "");
    return $html_table;
  }
  return t('<p>No Direct Report Information</p>');
}

function getLeaveRequests($sap_employee_id) {
  global $base_url;
  $wsdl_protocol  = variable_get('sap_hr_get_leave_request_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_get_leave_request_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_get_leave_request_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_get_leave_request_sap_request_xml', '');

  $response_arr = soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  $requests_org_arr = $response_arr['soap-env:body']['n0:employeeleaverequestbyparticipantresponse'];

  if (sizeof($requests_org_arr) > 2 && $requests_org_arr['0'] != '') {
    $header = array('Date', 'Status', 'Administrator', 'Initiator', 'Approver', 'Type', 'Type Description', 'Start Date', 'End Date', 'Calendar Days', 'Action');
    $requests = array();
    for ($i = 2; $i < sizeof($requests_org_arr) - 2; $i++) {
      $requesti = $requests_org_arr[strval($i - 2)];
      if ($requesti != '' && sizeof($requesti) > 0) {
        $requests[] = array($requesti['2']['data'] . ' ' . $requesti['2']['TIMEZONECODE'], 
                            $requesti['4']['data'],
                            $requesti['7']['formattedname'],
                            $requesti['8']['formattedname'],
                            $requesti['9']['formattedname'],
                            $requesti['10']['1']['data'],
                            $requesti['10']['3']['data'],
                            $requesti['10']['4']['dateperiod']['startdate'],
                            $requesti['10']['4']['dateperiod']['enddate'],
                            $requesti['10']['7']['duration'],
                            '<a href="' . $base_url . '/sapmanager/approve/' . $requesti['0'] . '">Approve</a>');
      }
    }
    $html_table = theme_table($header, $requests, "");
    return $html_table;
  }
  return t('No Leave Requests');
}

function getManagerInformation($sap_employee_id) {
  $wsdl_protocol  = variable_get('sap_hr_manager_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_manager_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_manager_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_manager_sap_request_xml', '');

  $response_arr = soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  $manager_org_arr = $response_arr['soap-env:body']['n0:reportinglinemanagersimplebyemployeeresponse']['0'];
  if ($manager_org_arr != '') {
    $manager[] = array($manager_org_arr['id'], $manager_org_arr['personformattedname']);
    $html_table = theme_table(array('Manager ID', 'Manager Name'), $manager, "");
    return $html_table;
  }
  return t('No Manager Information');
}

function getVacationBalanceInformation($sap_employee_id) {
  $wsdl_protocol  = variable_get('sap_hr_vacation_balance_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_vacation_balance_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_vacation_balance_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_vacation_balance_sap_request_xml', '');

  $response_arr = soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml);
  $vacation_balance_org_arr0 = $response_arr['soap-env:body']['n0:employeetimeaccountforemployeebyemployeeresponse']['0'];
  $vacation_balance_org_arr1 = $response_arr['soap-env:body']['n0:employeetimeaccountforemployeebyemployeeresponse']['1'];
  //$header = array('Processing Period', 'Deduction Period', 'Entitlement', 'Current Used', 'Approved', 'Requested', 'Planned', 'Current Remaining', 'Remaining', 'Available');
  $header = array('Processing Period', 'Deduction Period', 'Entitlement', 'Current Used', 'Approved', 'Requested', 'Planned', 'Available');
  if (sizeof($vacation_balance_org_arr0) > 0) {
    $processing_date = $vacation_balance_org_arr0['6']['processingperiod']['startdate'] . ' - ' . 
                       $vacation_balance_org_arr0['6']['processingperiod']['enddate'];
    $deduction_date = $vacation_balance_org_arr0['6']['deductionperiod']['startdate'] . ' - ' . 
                      $vacation_balance_org_arr0['6']['deductionperiod']['enddate'];
    $vacation_balance[] = array($processing_date, $deduction_date, $vacation_balance_org_arr0['7']['quantity'], 
                                $vacation_balance_org_arr0['8']['quantity'], $vacation_balance_org_arr0['9']['quantity'], 
                                $vacation_balance_org_arr0['10']['quantity'], $vacation_balance_org_arr0['11']['quantity'], 
                                //$vacation_balance_org_arr0['12']['quantity'], $vacation_balance_org_arr0['13']['quantity'], 
                                $vacation_balance_org_arr0['14']['quantity'] );
    if (sizeof($vacation_balance_org_arr1) > 0) {
    $processing_date = $vacation_balance_org_arr1['6']['processingperiod']['startdate'] . ' - ' .
                       $vacation_balance_org_arr1['6']['processingperiod']['enddate'];
    $deduction_date = $vacation_balance_org_arr1['6']['deductionperiod']['startdate'] . ' - ' . 
                      $vacation_balance_org_arr1['6']['deductionperiod']['enddate'];
    $vacation_balance[] = array($processing_date, $deduction_date, $vacation_balance_org_arr1['7']['quantity'], 
                                $vacation_balance_org_arr1['8']['quantity'], $vacation_balance_org_arr1['9']['quantity'], 
                                $vacation_balance_org_arr1['10']['quantity'], $vacation_balance_org_arr1['11']['quantity'], 
                                //$vacation_balance_org_arr1['12']['quantity'], $vacation_balance_org_arr1['13']['quantity'], 
                                $vacation_balance_org_arr1['14']['quantity'] );
    }
    $html_table = theme_table($header, $vacation_balance, "");
    return $html_table;
  }
  return t('No Vacation Balance Information');
}

// SOAP Get SAP Employee Information
function soapclient_invoke_get_emp_info($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml) {
  $username = variable_get('sap_hr_user_name', '');
  $password = variable_get('sap_hr_user_password', '');

  if ($username == '' || $wsdl_url == '' || $soap_action == '' || $org_request_xml == '') {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => 'Not configured properly'))), 'error');
    return;
  }
  if ($sap_employee_id == '') {
    $sap_employee_id = 1000;
  }

  // $replaced_request_xml = str_replace("1000", strval($sap_employee_id), $org_request_xml);
  // TODO: Integrate with token module
  $replace_request_xml = '';
  for ($i = 0; $i < strlen($org_request_xml); $i++) {
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '!') {
      $replace_request_xml[] = strval($sap_employee_id);
      $i = $i + 2;
    }
    $replace_request_xml[] = $org_request_xml[$i];
  }
  $converted_string = implode("", $replace_request_xml);
  
  $options = array();
  $options['namespace'] = '';
  $options['use']       = 'encoded';
  $options['style']     = 'rpc';  
  
  $result = sap_soapclient_init_client($wsdl_protocol . '://' . $username . ':' . $password . '@' . $wsdl_url, $username, $password, TRUE, $options);
  
  if ( $result['#error'] !== FALSE ) {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => $result['#error']))), 'error');
    return;
  }

  $client = $result['#return']->getClient();
  $response = $client->send($converted_string, $soap_action, '');
  //$result = $result['#return']->call($form['function']['#value'], $args);
  //$return_val = sap_soapclient_parse_xml_result($client->responseData)['#return']['SOAP-ENV:ENVELOPE'];;
  $return_val = sap_xml_parser_string_to_array($client->responseData);
  return $return_val;
}

function soapclient_invoke_approve_leave_request($leave_request_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml) {
  $username = variable_get('sap_hr_user_name', '');
  $password = variable_get('sap_hr_user_password', '');

  if ($username == '' || $wsdl_url == '' || $soap_action == '' || $org_request_xml == '' || $leave_request_id == '') {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => 'Not configured properly'))), 'error');
    return;
  }

  //$replaced_request_xml = str_replace("1000", strval($sap_employee_id), $org_request_xml);
  $replace_request_xml = '';
  for ($i = 0; $i < strlen($org_request_xml); $i++) {
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '1') {
      $replace_request_xml[] = strval($leave_request_id);
      $i = $i + 2;
    }
    $replace_request_xml[] = $org_request_xml[$i];
  }
  $converted_string = implode("", $replace_request_xml);
  
  $options = array();
  $options['namespace'] = '';
  $options['use']       = 'encoded';
  $options['style']     = 'rpc';  
  
  $result = sap_soapclient_init_client($wsdl_protocol . '://' . $username . ':' . $password . '@' . $wsdl_url, $username, $password, TRUE, $options);
  
  if ( $result['#error'] !== FALSE ) {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => $result['#error']))), 'error');
    return;
  }

  $client = $result['#return']->getClient();
  $response = $client->send($converted_string, $soap_action, '');
  //$result = $result['#return']->call($form['function']['#value'], $args);
  //$return_val = sap_soapclient_parse_xml_result($client->responseData)['#return']['SOAP-ENV:ENVELOPE'];;
  $return_val = sap_xml_parser_string_to_array($client->responseData);
  return $return_val;
}
