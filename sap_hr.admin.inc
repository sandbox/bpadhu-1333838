<?php

/**
 * @file
 * SAP HR SOAP module administration menu and callbacks
 *
 */

/**
 * Default SAP HR API settings.
 *
 * Records SAP HR Employee related information neccessary to use service.
 *
 * @return
 *   Forms for administrator to set configuration options.
 */
function sap_hr_basic_settings() {
  $form['sap_hr_basic_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Credentials',
    '#description'  => 'User name and password information. <br>Can use demo username: "sap_esw" and demo password: "eswork123"',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the user information.
  $form['sap_hr_basic_information']['sap_hr_user_name'] = array(
    '#type'          => 'textfield',
    '#title'         => 'User name to login to SAP HR',
    '#default_value' => variable_get('sap_hr_user_name', ''),
    '#required'      => TRUE,
  );

  // the user password.
  $form['sap_hr_basic_information']['sap_hr_user_password'] = array(
    '#type'          => 'password',
    '#title'         => 'User password to login to SAP HR',
    '#default_value' => variable_get('sap_hr_user_password', ''),
    '#required'      => TRUE,
  );
  $form['#validate'][] = 'sap_hr_admin_settings_validate';

  return system_settings_form($form);
}

/**
 * Validation handler for sap_hr_admin_settings_validate form.
 *
 * Require password only if it hasn't been set.
 *
 */
function sap_hr_admin_settings_validate($form, &$form_state) {
  $old_password = variable_get('sap_hr_user_password', '');
  if (!$form_state['values']['sap_hr_user_password']) {
    if ($old_password) {
      form_set_value($form['sap_hr_basic_information']['sap_hr_user_password'], $old_password, $form_state);
    }
    else {
      form_set_error('sap_hr_user_password', t('Password field is required.'));
    }
  }
}

function sap_hr_basic_employee_admin_settings() {
  $form['sap_hr_employee_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service Information',
    '#description'  => 'Web service Information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_employee_information']['sap_hr_employee_information_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR wsdl',
    '#default_value' => variable_get('sap_hr_employee_information_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_employee_information']['sap_hr_employee_information_sap_wsdl_url'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Url without the protocol to SAP HR Employee Information WSDL',
    '#default_value' => variable_get('sap_hr_employee_information_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_employee_information']['sap_hr_employee_information_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Employee Information WSDL',
    '#default_value' => variable_get('sap_hr_employee_information_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate 
  // namespace for individual elements, we just take xml.
  $form['sap_hr_employee_information']['sap_hr_employee_information_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information WSDL'),
    '#default_value' => variable_get('sap_hr_employee_information_sap_request_xml', ''),
    '#required'      => TRUE,
  );

  return system_settings_form($form);
}

function sap_hr_vacation_balance_admin_settings() {
  $form['sap_hr_vacation_balance_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service information',
    '#description'  => 'Web service information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_vacation_balance_information']['sap_hr_vacation_balance_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Vacation Balance wsdl',
    '#default_value' => variable_get('sap_hr_vacation_balance_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_vacation_balance_information']['sap_hr_vacation_balance_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Vacation Balance WSDL',
    '#default_value' => variable_get('sap_hr_vacation_balance_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_vacation_balance_information']['sap_hr_vacation_balance_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Vacation Balance WSDL',
    '#default_value' => variable_get('sap_hr_vacation_balance_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_vacation_balance_information']['sap_hr_vacation_balance_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Vacation Balance WSDL'),
    '#default_value' => variable_get('sap_hr_vacation_balance_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}

function sap_hr_create_leave_request_admin_settings() {
  $form['sap_hr_create_leave_request_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service information',
    '#description'  => 'Web service information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_create_leave_request_information']['sap_hr_create_leave_request_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Create Leave Request wsdl',
    '#default_value' => variable_get('sap_hr_create_leave_request_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_create_leave_request_information']['sap_hr_create_leave_request_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Create Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_create_leave_request_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_create_leave_request_information']['sap_hr_create_leave_request_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Create Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_create_leave_request_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_create_leave_request_information']['sap_hr_create_leave_request_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Create Leave Request WSDL'),
    '#default_value' => variable_get('sap_hr_create_leave_request_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}

function sap_hr_manager_admin_settings() {
  $form['sap_hr_manager_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service information',
    '#description'  => 'Web service information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_manager_information']['sap_hr_manager_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Manager wsdl',
    '#default_value' => variable_get('sap_hr_manager_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_manager_information']['sap_hr_manager_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Manager WSDL',
    '#default_value' => variable_get('sap_hr_manager_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_manager_information']['sap_hr_manager_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Manager WSDL',
    '#default_value' => variable_get('sap_hr_manager_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_manager_information']['sap_hr_manager_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Manager WSDL'),
    '#default_value' => variable_get('sap_hr_manager_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}

function sap_hr_direct_reports_admin_settings() {
  $form['sap_hr_direct_report_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service Information',
    '#description'  => 'Web service Information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_direct_report_information']['sap_hr_direct_report_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Direct Report wsdl',
    '#default_value' => variable_get('sap_hr_direct_report_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_direct_report_information']['sap_hr_direct_report_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Direct Report WSDL',
    '#default_value' => variable_get('sap_hr_direct_report_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_direct_report_information']['sap_hr_direct_report_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Direct Report WSDL',
    '#default_value' => variable_get('sap_hr_direct_report_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_direct_report_information']['sap_hr_direct_report_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Direct Report WSDL'),
    '#default_value' => variable_get('sap_hr_direct_report_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}

function sap_hr_get_leave_request_admin_settings() {
  $form['sap_hr_get_leave_request_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service information',
    '#description'  => 'Web service information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_get_leave_request_information']['sap_hr_get_leave_request_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Get Leave Request wsdl',
    '#default_value' => variable_get('sap_hr_get_leave_request_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_get_leave_request_information']['sap_hr_get_leave_request_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Get Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_get_leave_request_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_get_leave_request_information']['sap_hr_get_leave_request_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Get Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_get_leave_request_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_get_leave_request_information']['sap_hr_get_leave_request_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Get Leave Request WSDL'),
    '#default_value' => variable_get('sap_hr_get_leave_request_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}

function sap_hr_approve_leave_request_admin_settings() {
  $form['sap_hr_approve_leave_request_information'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Web service information',
    '#description'  => 'Web service information',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // the wsdl protocol.
  $form['sap_hr_approve_leave_request_information']['sap_hr_approve_leave_request_sap_wsdl_protocol'] = array(
    '#type'          => 'select',
    '#title'         => 'Protocol to access SAP HR Approve Leave Request wsdl',
    '#default_value' => variable_get('sap_hr_approve_leave_request_sap_wsdl_protocol', 'http'),
    '#options'     => array(
      'http'    => t('Http'),
      'https'   => t('Https'),
    ),
    '#description'   => 'Choose one of the protocols to access the WSDL',
  );

  // the wsdl url.
  $form['sap_hr_approve_leave_request_information']['sap_hr_approve_leave_request_sap_wsdl_url'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Url without the protocol to SAP HR Approve Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_approve_leave_request_sap_wsdl_url', ''),
    '#required'      => TRUE,
  );

  // the soap action.
  $form['sap_hr_approve_leave_request_information']['sap_hr_approve_leave_request_sap_soap_action'] = array(
    '#type'          => 'textfield',
    '#title'         => 'SOAP Action to invoke in the SAP HR Approve Leave Request WSDL',
    '#default_value' => variable_get('sap_hr_approve_leave_request_sap_soap_action', ''),
    '#required'      => TRUE,
  );

  // the soap request xml - since we cannot populate appropriate namespace for individual elements,
  // we just take xml.
  $form['sap_hr_approve_leave_request_information']['sap_hr_approve_leave_request_sap_request_xml'] = array(
    '#type'          => 'textarea',
    '#title'         => 'Complete Request XML including arguments',
    '#default_value' => t('Complete Request XML including arguments to invoke the Soap Action in the SAP HR Employee Information Approve Leave Request WSDL'),
    '#default_value' => variable_get('sap_hr_approve_leave_request_sap_request_xml', ''),
    '#required'      => TRUE,
  );
  return system_settings_form($form);
}