<?php

/**
 * @file
 * SAP HR SOAP module user menu and callbacks
 */

/**
 * Default SAP HR API settings.
 *
 * Records SAP HR Employee related information neccessary to use service.
 *
 * @return
 *   Forms for store administrator to set configuration options.
 */
function sap_hr_create_leave_request_user() {
  // current user, manager, start-date, end-date, type - 0100(vacation) or 0200(sick leave), 
  // category - 3 for vacation and sick leave.
  // Container for employee information webservice.
  $form['sap_hr_create_leave_request_user'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Create Leave Request',
    '#description'  => 'Create and Submit Leave Request',
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  $today = date("Y-m-d");
  $format = "Y-m-d";
  $form['sap_hr_create_leave_request_user']['startdate'] = array(
    '#title' => 'From Date',
    '#type' => 'date_popup',
    '#default_value' => $today,
    '#date_format' => $format,
  );
  
  $form['sap_hr_create_leave_request_user']['enddate'] = array(
    '#title' => 'End Date',
    '#type' => 'date_popup',
    '#default_value' => $today,
    '#date_format' => $format,
  );

  $form['sap_hr_create_leave_request_user']['category'] = array(
    '#title' => 'Type',
    '#type' => 'select',
    '#default_value' => '0100',
    '#options' => array('0100' => 'Vacation', '0200' => 'Sick Leave'),
  );

  $form['sap_hr_create_leave_request_user']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function sap_hr_create_leave_request_user_submit($form, &$form_state) {
  $wsdl_protocol  = variable_get('sap_hr_create_leave_request_sap_wsdl_protocol', 'http');
  $wsdl_url    = variable_get('sap_hr_create_leave_request_sap_wsdl_url', '');
  $soap_action = variable_get('sap_hr_create_leave_request_sap_soap_action', '');
  $org_request_xml = variable_get('sap_hr_create_leave_request_sap_request_xml', '');

  $start_date = substr($form_state['values']['startdate'], 0, 10);
  $end_date = substr($form_state['values']['enddate'], 0, 10);
  $category = $form_state['values']['category'];

  $sap_employee_id = '1000';
  if ($form['#parameters']['3'] != '')
    $sap_employee_id = $form['#parameters']['3'];
  
  $response_arr = soapclient_invoke_submit_leave_request($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml, $start_date, $end_date, $category);
}

function soapclient_invoke_submit_leave_request($sap_employee_id, $wsdl_protocol, $wsdl_url, $soap_action, $org_request_xml, $start_date, $end_date, $category) {
  $username = variable_get('sap_hr_user_name', '');
  $password = variable_get('sap_hr_user_password', '');

  if ($username == '' || $wsdl_url == '' || $soap_action == '' || $org_request_xml == '') {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => 'Not configured properly'))), 'error');
    return;
  }
  if ($sap_employee_id == '') {
    $sap_employee_id = 1000;
  }

  //$replaced_request_xml = str_replace("1000", strval($sap_employee_id), $org_request_xml);
  $replace_request_xml = '';
  for ($i = 0; $i < strlen($org_request_xml); $i++) {
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '1') {
      $replace_request_xml[] = strval($sap_employee_id);
      $i = $i + 2;
    }
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '2') {
      $replace_request_xml[] = strval($category);
      $i = $i + 2;
    }
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '3') {
      $replace_request_xml[] = strval($start_date);
      $i = $i + 2;
    }
    if ((($i + 1) < strlen($org_request_xml)) && $org_request_xml[$i] == '!' && $org_request_xml[$i + 1] == '4') {
      $replace_request_xml[] = strval($end_date);
      $i = $i + 2;
    }
    $replace_request_xml[] = $org_request_xml[$i];
  }
  $converted_string = implode("", $replace_request_xml);
  
  $options = array();
  $options['namespace'] = '';
  $options['use']       = 'encoded';
  $options['style']     = 'rpc';  
  
  $result = sap_soapclient_init_client($wsdl_protocol . '://' . $username . ':' . $password . '@' . $wsdl_url, $username, $password, TRUE, $options);
  
  if ( $result['#error'] !== FALSE ) {
    drupal_set_message(check_plain(t('Error: !msg', array('!msg' => $result['#error']))), 'error');
    return;
  }

  $client = $result['#return']->getClient();
  $response = $client->send($converted_string, $soap_action, '');
  //$result = $result['#return']->call($form['function']['#value'], $args);
  //$return_val = sap_soapclient_parse_xml_result($client->responseData)['#return']['SOAP-ENV:ENVELOPE'];;
  $return_val = sap_xml_parser_string_to_array($client->responseData);
  return $return_val;
}
